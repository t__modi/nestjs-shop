const checksession = function () {
  if ("token" in localStorage) {
    $(".loggedin").toggleClass("dnone");
    $("#name").html(localStorage.getItem("name"));
  }
}
const checkadmin = function () {
  var admin = localStorage.getItem("isadmin")
  if (admin == "true") {
    $(".admin").removeClass("dnone");
  } else {
    $(".admin").addClass("dnone");
  }
}
const showerror = function (message) {
  var errortoast = `<div class="alert alert-danger alert-dismissible fade show" id="alert" style="z-index:2000">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<strong>Error!</strong> ${message}
  </div>`;
  $("#alert").remove();
  $(".deck").before(errortoast);
  setTimeout(() => {
    $("#alert").fadeOut();
  }, 5000)
}


const successmessage = function (message) {
  var successtoast = `<div class="alert alert-success alert-dismissible fade show" id="alert">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<strong>Success!</strong> ${message}
  </div>`;
  $("#alert").remove();
  $(".deck").before(successtoast);
  setTimeout(() => {
    $("#alert").fadeOut();
  }, 5000)
}
const editModal=function(product){
var content=`<div class="modal fade" id="editproductmodal">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">
        <p class="navbar-brand"><b>MTX</b>SHOP</p>
      </h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
      <form id="editproduct" method="POST" enctype="multipart/form-data">
        <div class="form-group">
          <label for="Title">Title</label>
          <input type="text" class="form-control" value="${product.title}"
            name="Title" required>
        </div>
        <div class="form-group">
          <label for="Price">Product Price</label>
          <input type="number" class="form-control" value=${product.price} name="Price" required>
        </div>
        <div class="form-group">
          <label for="Description">Description</label>
          <input type="text" class="form-control" value="${product.description}"
            name="Description" required>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>`
$("nav").after(content);
  $("#editproductmodal").modal('show');

  $("#editproduct").submit(function (e) {
    e.preventDefault();
    $("#editproductmodal").modal('hide');
    var inputs = $('#editproduct :input');
    var values = {};
    inputs.each(function () {
      values[this.name] = this.value;
    });
    updateproduct(values,product._id);
  });
}


const detailModal = function (element) {
  var content = `<div class="modal fade" id="productdetails">
	<div class="modal-dialog modal-side modal-l" role="document">
	  <div class="modal-content">
    <div class="modal-header">
     <h2 ><p class="heading">${element.title}</p></h2>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true" class="white-text">&times;</span>
      </button>
		</div>
		<div class="modal-body">
  			<div class="row">
			<div class="col-5">
			  <img src="./8.jpeg"
				class="img-fluid" alt="">
			</div>
  			<div class="col-7">
      <div class="row price"><p class="mx-auto"><b>Price:$ </b><strong>${element.price}</strong></p></div>
			  <div class="row" style="color:#33cabb"><p><strong>Description:</strong></p></div>
			  <div class="row"><p>${element.description}</p></div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
`
  $("nav").after(content);
  $("#productdetails").modal('show');
}
const loadProduct = function (element) {
  var elem = `<div class="card col-auto mx-4 mt-5" id="${element._id}">
    <div><Span class="editprod admin dnone" onclick="editprod(this)">
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
    <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
  </svg>
    </Span></div>
    <img class="card-img-top mx-auto mt-4 img-responsive img-circle" src="./8.jpeg" alt="Card image"/>
    <div class="card-body">
      <h4 class="card-title">${element.title}</h4>
      <p class="price"><b>$</b>${element.price}</p>
      <a href="#" class="btn btn-primary" id="details" onClick="showdetails(this)">See Details</a>
      <a href="#" class="btn btn-primary admin dnone dangerbtn" id="delete" onClick="deleteprod(this)">Delete</a>
    </div>
</div>`;
  $(".deck").prepend(elem);
  checkadmin();
}


// get all products
const getAllProducts = function () {
  fetch('http://localhost:3000/products')
    .then(res => {
      if (res.status !== 200) {
        throw new Error('Failed to fetch Product');
      }
      return res.json();
    })
    .then(resData => {
      $(".deck").empty();
      if(resData.length != 0){
      resData.forEach((elem) => { loadProduct(elem) });
      } else{
        showerror('No Products found');
      }
    })
    .catch(err => {
      showerror(err)
    });
}



// signup call
const signup = function (values) {
  fetch('http://localhost:3000/auth/signup', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email: values.Email,
      password: values.Password.toString(),
      name: values.Username,
      isadmin: values.isadmin
    })
  })
    .then(res => {
      if (res.status === 422) {
        throw new Error(res.message);
      }
      if (res.status !== 200 && res.status !== 201) {
        throw new Error('Creating a user failed!');
      }
      return res;
    })
    .then(resData => {
      successmessage('Signup Successful,Login to continue');
      $("#form-signup")[0].reset();
    })
    .catch(err => {
      showerror(err)
    });
}



// Login call
const login = function (values) {
  fetch('http://localhost:3000/auth/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email: values.Email,
      password: values.Password.toString()
    })
  })
    .then(res => {
      if (res.status === 422) {
        throw new Error(res.message);
      }
      if (res.status !== 200 && res.status !== 201) {
        throw new Error('Login Failed');
      }
      return res.json();
    })
    .then(resData => {
      localStorage.setItem('token', resData.token);
      localStorage.setItem('userId', resData.userId);
      localStorage.setItem('isadmin', resData.isadmin);
      localStorage.setItem('name', resData.name);
      successmessage('Login Successfull');
      $(".loggedin").toggleClass("dnone");
      $("#name").html(resData.name);
      if (resData.isadmin) {
        $(".admin").toggleClass("dnone");
        $("#form-login")[0].reset();
      }
    })
    .catch(err => {
      showerror(err);
    });

}


// create product call
const addproduct = function (values) {
  const formdata = new FormData();
  // // formdata.append('image', values.image);
  // formdata.append('title', values.Title);
  // formdata.append('description', values.Description);
  // formdata.append('price', values.Price);
  // formdata.append('userId', localStorage.getItem("userId"));

  fetch('http://localhost:3000/products', {
    method: "POST",
    body: JSON.stringify({
      title: values.Title,
      description: values.Description,
      price: values.Price
    }),
    headers: {
      Authorization: 'Bearer ' + localStorage.getItem("token"),
      'Content-Type': 'application/json'
    }
  })
    .then(res => {
      if (res.status !== 200 && res.status !== 201) {
        throw new Error('Creating a post failed!');
      }
      return res.json();
    })
    .then(resData => {
      successmessage(resData.msg);
      loadProduct(resData.product);
      $("#product")[0].reset();
    })
    .catch(err => {
      showerror(err);
    });
}

// delete call
const deleteprod = function (element) {
  prodid = $(element).parent().parent().attr("id");
  fetch('http://localhost:3000/products/' + prodid, {
    method: 'DELETE',
    headers: {
      Authorization: 'Bearer ' + localStorage.getItem("token")
    }
  })
    .then(res => {
      if (res.status !== 200 && res.status !== 201) {
        throw new Error('Deleting a post failed!');
      }
      return res;
    })
    .then(resData => {
      $("#" + prodid).remove();
    })
    .catch(err => {
      showerror(err);
    });
}

const showdetails=async function(elem){
  getSingleProduct(elem,detailModal)
}

//show single product
const getSingleProduct = function (elem,callback) {
  prodid = $(elem).parent().parent().attr("id");
  fetch('http://localhost:3000/products/' + prodid,)
    .then(res => {
      if (res.status !== 200) {
        throw new Error('Failed to fetch Product');
      }
      return res.json();
    }).then(res=>{
      callback(res);
    })
    .catch(err => {
      showerror(err);
    });

}

// user products
var getuserProducts = function () {
  if (localStorage.getItem("userId") === null) {
    window.location.reload();
    throw new Error('Session expired,Login again!!!!');
  }
  fetch('http://localhost:3000/products/userptoducts/' + localStorage.getItem('userId'), {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + localStorage.getItem("token")
    }
  })
    .then(res => {
      if (res.status !== 200) {
        throw new Error('Failed to fetch user Product.');
      }
      return res.json();
    })
    .then(resData => {
      $(".deck").empty();
      if(resData.length!=0){
      resData.forEach((elem) => { loadProduct(elem) });
      }
      else{
        showerror('No Products found');
      }
    })
    .catch(err => {
      showerror(err);
    });
}



const editprod=async function(elem){
  getSingleProduct(elem,editModal);
}
const updateproduct=function(values,prodid){
    fetch('http://localhost:3000/products/' + prodid,{
    method: "PATCH",
    body: JSON.stringify({
    title: values.Title,
    description: values.Description,
    price: values.Price
  }),
  headers: {
    Authorization: 'Bearer ' + localStorage.getItem("token"),
    'Content-Type': 'application/json'
  }
})
    .then(res => {
      if (res.status !== 200) {
        throw new Error('Failed to fetch Product');
      }
      return res;
    })
    .then(resData => {
      successmessage("Product Update Successful");
      getAllProducts();
    })
    .catch(err => {
      showerror(err);
    });
}

// ============================================entry point===================================================================

$(document).ready(function () {
  $(document).on("click", ".action-buttons .dropdown-menu", function (e) {
    e.stopPropagation();
  });
  $('#navbtn').click(function() {
    $(this).parents('.dropdown').find('button.dropdown-toggle').dropdown('toggle');
  });
  checksession();
  getAllProducts();
 

  //signup 
  $("#form-signup").submit(function (e) {
    e.preventDefault();
    $("#navsignup").dropdown('toggle');
    var val = $("input[type=submit][clicked=true]").val();
    var inputs = $('#form-signup :input');
    var values = {};
    inputs.each(function () {
      values[this.placeholder] = this.value;
    });
    if (val == "Sign up") {
      values["isadmin"] = false;
    } else {
      values["isadmin"] = true;
    }
    if(values["Confirm Password"]===values.Password){
      signup(values);
    }
    else{
      showerror('password does not match');
    }
  });

  $("form input[type=submit]").click(function () {
    $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
    $(this).attr("clicked", "true");
  });

  // Login
  $("#form-login").submit(function (e) {
    e.preventDefault();
    var inputs = $('#form-login :input');
    var values = {};
    inputs.each(function () {
      values[this.placeholder] = this.value;
    });
    login(values);
  });


  // signout
  $("#signout").click(() => {
    localStorage.removeItem('token');
    localStorage.removeItem('isadmin');
    localStorage.removeItem('userId');
    localStorage.removeItem('name');
    window.location.reload();
  })

  // add product
  $("#addproduct").click(() => {
    $("#productModal").modal('toggle');
  })
  $("#product").submit(function (e) {
    e.preventDefault();
    $("#productModal").modal('hide');
    var inputs = $('#product :input');
    var values = {};
    inputs.each(function () {
      values[this.name] = this.value;
    });
    // values["image"] = $('#img')[0].files[0];
    addproduct(values);
  });

// get user product
  $("#userproducts").click(() => {
    getuserProducts();
  })

  // back to home
  $(".navbar-brand").click(()=>{
    window.location.reload();
  })
});
