import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { UserController } from './user.controller';
import { UsersSchema } from './user.model';
import { UserService } from './user.service';
import { AuthModule } from '../../auth/auth.module';
import { AuthService } from 'src/auth/auth.service';
import { JwtService } from '@nestjs/jwt';

@Module({
    imports: [
        AuthModule,
        MongooseModule.forFeature([{ name: 'User', schema: UsersSchema }]),  
      ],
      controllers: [UserController],
      providers: [UserService]
})
export class UsersModule {}
