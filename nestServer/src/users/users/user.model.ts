import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import {Product} from './../../products/product.model'
import { IsEmail, MinLength} from 'class-validator'

export class UserDto{
 readonly id: string;
 @IsEmail()
 readonly email: string;
 @MinLength(5)
 readonly password: string;
 readonly name: String;
 readonly isadmin: Boolean;
 readonly products: Product[];
}

@Schema()
export class Users extends Document{
    @Prop({unique:true,required: true})
    email: string;
    
    @Prop({required: true})
    password: String;
      
    @Prop({required:true})
    name: String;
       
    @Prop({default:false})
    isadmin: Boolean;

    @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Product' }] })
    products:Product[]
}

export const UsersSchema = SchemaFactory.createForClass(Users);