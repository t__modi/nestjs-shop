import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AuthService } from 'src/auth/auth.service';
import { UserDto, Users } from './user.model';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt')

@Injectable()
export class UserService {

  constructor(
    private readonly authService: AuthService,
    @InjectModel('User') private readonly userModel: Model<Users>
  ) { }

  //Service for user Signup
  async signup(user: UserDto) {
    const hashedPassword = await bcrypt.hash(user.password, 12)
    const newUser = new this.userModel({
      email: user.email,
      password: hashedPassword,
      name: user.name,
      isadmin: user.isadmin
    });
    await newUser.save();
    return 'user Created successfully';
  }

  //Service for user Login
  async login(userData: UserDto) {
    let userToAttempt = await this.userModel.findOne({ email: userData.email });
    if (!userToAttempt) {
      throw new NotFoundException('Invalid email address');
    }
    const isMatch = await bcrypt.compare(userData.password, userToAttempt.password);
    if (!isMatch) {
      throw new UnauthorizedException('Invalid Password');
    }
    const token = await this.authService.login(userToAttempt);
    return { name: userToAttempt.name, token: token, isadmin: userToAttempt.isadmin, userId: userToAttempt._id };
  }

}
