import {Controller,Post,Body,Get,Param,Patch,Delete, Put, UseGuards,} from '@nestjs/common';
import { UserDto } from './user.model';
import {UserService} from './user.service'


@Controller('auth')
export class UserController {

    constructor(private readonly userService: UserService) { }

    @Put('signup')
    async signup(@Body() user:UserDto) {
       return await this.userService.signup(user)
      }
    
    @Post('login')
    async login(@Body() userData:UserDto){
        return await this.userService.login(userData);
    }
}
