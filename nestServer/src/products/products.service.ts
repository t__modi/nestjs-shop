import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as mongoose from 'mongoose';
import { Users } from 'src/users/users/user.model';

import { Product, ProductDto } from './product.model';

@Injectable()
export class ProductsService {
  
  constructor(
    @InjectModel('Product') private readonly productModel: Model<Product>,
    @InjectModel('User') private readonly userModel: Model<Users>
  ) { }

  async getProducts() {
    const products = await this.productModel.find().exec();
    return products;
  }

  async getSingleProduct(productId: string) {
    const product = await this.findProduct(productId);
    return product;
  }


  async insertProduct(product: ProductDto, userId: any) {

    /* Example of mongodb transaction */
    const session = await this.productModel.db.startSession();
    session.startTransaction();
    try {
      const newProduct = new this.productModel({ ...product, User: userId });
      var result = await newProduct.save();
      var Owner = await this.userModel.findById(userId);
      Owner.products.push(result._id);
      await Owner.save();
      await session.commitTransaction();
    } catch (error) {
      await session.abortTransaction();
    } finally {
      session.endSession();
      return result;
    }
  }

  async updateProduct(
    productId: string,
    title: string,
    desc: string,
    price: number,
  ) {
    const updatedProduct = await this.findProduct(productId);
    if (title) {
      updatedProduct.title = title;
    }
    if (desc) {
      updatedProduct.description = desc;
    }
    if (price) {
      updatedProduct.price = price;
    }
    await updatedProduct.save();
    return 'Product Update Successful';
  }



  async deleteProduct(prodId: string) {

    /* Example of mongodb transaction */

    const session = await this.productModel.db.startSession();
    session.startTransaction();
    try {
      const product = await this.findProduct(prodId);
      if (!product) {
        throw new NotFoundException('Could not find product.');
      }
      var Owner = await this.userModel.findById(product.User).session(session).exec();
      if (!Owner.isadmin) {
        throw new UnauthorizedException('Only admin can delete');
      }
      Owner.products.splice(Owner.products.indexOf(product._id), 1);
      await Owner.save();
      var result = await this.productModel.findByIdAndRemove(prodId).session(session).exec();
      await session.commitTransaction();
    } catch (error) {
      await session.abortTransaction();
    } finally {
      session.endSession();
      return result;
    }
  }

  async getUserProducts(userId: any) {
    const user = await this.userModel.findById(userId)
    return await this.productModel.find({ '_id': { $in: user.products } })
  }


  //utility method
  private async findProduct(id: string): Promise<Product> {
    let product;
    try {
      product = await this.productModel.findById(id).exec();
    } catch (error) {
      throw new NotFoundException('Could not find product.');
    }
    if (!product) {
      throw new NotFoundException('Could not find product.');
    }
    return product;
  }
}










