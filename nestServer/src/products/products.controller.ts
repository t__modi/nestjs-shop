import {Controller,Post,Body,Get,Param,Patch,Delete, UseGuards, Request} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ProductDto } from './product.model';

import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) { }
  
  //Public Route to return All products
  @Get()
  async getAllProducts() {
    const products = await this.productsService.getProducts();
    return products;
  }
  
  //Public Route to return single product detail
  @Get(':id')
  getProduct(@Param('id') prodId: string) {
    return this.productsService.getSingleProduct(prodId);
  }

  //Authorised Route to add new product
  @UseGuards(AuthGuard('jwt'))
  @Post()
  async addProduct(
    @Body() product: ProductDto,
    @Request() req
    ) {
    const generatedProduct = await this.productsService.insertProduct(product,req.user.id);
    return { msg:'Product created Successfully',product: generatedProduct };
  }

  //Authorised Route to update a product
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  async updateProduct(
    @Param('id') prodId: string,
    @Body('title') prodTitle: string,
    @Body('description') prodDesc: string,
    @Body('price') prodPrice: number,

  ) {
   return await this.productsService.updateProduct(prodId, prodTitle, prodDesc, prodPrice);
  }
  
  //Authorised Route to get products of a user
  @UseGuards(AuthGuard('jwt'))
  @Get('/userptoducts/:userId')
  async getuserproduct(
    @Param('userId') userId: string,
  ){
    return await this.productsService.getUserProducts(userId);
  }

  //Authorised Route to delete product
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async removeProduct(@Param('id') prodId: string) {
    await this.productsService.deleteProduct(prodId);
    return "Product Delete Successful";
  }
}
