import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { Users } from 'src/users/users/user.model';

export class ProductDto{
 readonly id: string;
 readonly title: string;
 readonly description: string;
 readonly price: number;
}

@Schema()
export class Product extends Document{
  @Prop({required :true})
  title:string;

  @Prop({required :true})
  description:string;

  @Prop({required :true})
  price:Number;

   @Prop({type:mongoose.Schema.Types.ObjectId,ref:'Users'})
   User:Users;


}

export const ProductSchema = SchemaFactory.createForClass(Product);